# Spring OO

### Description

This project aims to propose an architectural approach to embrace object-oriented programming practices in the context of a microservice application using Spring Boot.

### Main Goals
- Decouple the `Business/Service` layer of other concrete implementations such as `Requests` and `Responses`
- Isolate the persistence implementation in the `Repository` layer
- Isolate the HTTP implementation in the `Controller` layer
- Use Spring conventions for handling requests and responses
- Use Spring context and dependency injection to provide concrete implementations for the business layer
- Convert a Request object to a Business object in an elegant way
- Convert a Business object to a Response object in an elegant way
