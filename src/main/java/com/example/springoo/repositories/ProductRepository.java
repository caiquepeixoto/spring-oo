package com.example.springoo.repositories;

import com.example.springoo.product.domain.ProductJPA;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<ProductJPA, Long> { }
