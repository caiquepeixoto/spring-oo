package com.example.springoo.product.responses;

import com.example.springoo.product.domain.ContentId;
import com.example.springoo.product.domain.InterfaceThatDescribesABusinessResponse;

public class CreateNewProductResponse {
    private final String name;
    private final ContentId contentId;

    public CreateNewProductResponse(InterfaceThatDescribesABusinessResponse entity) {
        this.name = entity.getName();
        this.contentId = new ContentId(entity.getId());
    }

    public String getName() {
        return name;
    }

    public ContentId getContentId() {
        return contentId;
    }
}
