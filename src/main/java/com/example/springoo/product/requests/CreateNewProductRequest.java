package com.example.springoo.product.requests;

import com.example.springoo.product.domain.InterfaceToCreateAProduct;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateNewProductRequest implements InterfaceToCreateAProduct {

    private String name;

    public CreateNewProductRequest(@JsonProperty("name") String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
