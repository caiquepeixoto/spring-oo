package com.example.springoo.product.domain;

public interface InterfaceThatDescribesABusinessResponse {
    Long getId();
    String getName();
}
