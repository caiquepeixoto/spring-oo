package com.example.springoo.product.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "products")
public class ProductJPA {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public ProductJPA(InterfaceToCreateAProduct productable) {
        this.name = productable.getName();
    }

//    @Override
//    public String getName() {
//        return this.name;
//    }
//
//    @Override
//    public Long getId() {
//        return this.id;
//    }

    @PrePersist
    private void onCreate() {
        LocalDateTime now = LocalDateTime.now();
        this.createdAt = now;
        this.updatedAt = now;
    }

    @PreUpdate
    private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
}
