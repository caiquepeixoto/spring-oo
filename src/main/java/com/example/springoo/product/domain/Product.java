package com.example.springoo.product.domain;

public class Product implements InterfaceToCreateAProduct, InterfaceThatDescribesABusinessResponse {
    private final String name;

    public Product(InterfaceToCreateAProduct productable) {
        this.name = productable.getName();
    }

    @Override
    public Long getId() {
        return 1L;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
