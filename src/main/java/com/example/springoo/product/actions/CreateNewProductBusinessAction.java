package com.example.springoo.product.actions;

import com.example.springoo.product.domain.Product;
import com.example.springoo.product.domain.ProductJPA;
import com.example.springoo.product.domain.InterfaceToCreateAProduct;
import com.example.springoo.product.domain.InterfaceThatDescribesABusinessResponse;
import com.example.springoo.repositories.ProductRepository;
import org.springframework.stereotype.Service;

@Service
public class CreateNewProductBusinessAction {

    private ProductRepository productRepository;

    public CreateNewProductBusinessAction(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public InterfaceThatDescribesABusinessResponse actOn(InterfaceToCreateAProduct productable) {
        InterfaceToCreateAProduct product = new Product(productable);

        ProductJPA productAsEntity = new ProductJPA(product);

        ProductJPA savedEntity = productRepository.save(productAsEntity);
        InterfaceThatDescribesABusinessResponse productForResponse = new Product(product);

        return productForResponse;
    }
}
