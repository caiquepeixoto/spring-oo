package com.example.springoo.controllers;

import com.example.springoo.product.actions.CreateNewProductBusinessAction;
import com.example.springoo.product.domain.InterfaceThatDescribesABusinessResponse;
import com.example.springoo.product.requests.CreateNewProductRequest;
import com.example.springoo.product.responses.CreateNewProductResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
public class ProductController {

    private CreateNewProductBusinessAction createNewProductBusinessAction;

    public ProductController(CreateNewProductBusinessAction createNewProductBusinessAction) {
        this.createNewProductBusinessAction = createNewProductBusinessAction;
    }

    @RequestMapping(path = "products", method = RequestMethod.POST)
    public ResponseEntity<CreateNewProductResponse> create(@RequestBody CreateNewProductRequest request) {
        InterfaceThatDescribesABusinessResponse savedProduct = createNewProductBusinessAction.actOn(request);
        CreateNewProductResponse response = new CreateNewProductResponse(savedProduct);

        return new ResponseEntity<CreateNewProductResponse>(response, CREATED);
    }
}
