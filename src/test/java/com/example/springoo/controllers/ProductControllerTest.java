package com.example.springoo.controllers;

import com.example.springoo.product.actions.CreateNewProductBusinessAction;
import com.example.springoo.product.domain.InterfaceThatDescribesABusinessResponse;
import com.example.springoo.product.domain.InterfaceToCreateAProduct;
import com.example.springoo.product.requests.CreateNewProductRequest;
import com.example.springoo.product.responses.CreateNewProductResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.CREATED;

public class ProductControllerTest {

    private ProductController productController;

    @Mock
    private CreateNewProductBusinessAction createNewProductBusinessAction;

    @Mock
    private InterfaceThatDescribesABusinessResponse mockedResponseFromBusinessAction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.productController = new ProductController(createNewProductBusinessAction);
        when(createNewProductBusinessAction.actOn(any(InterfaceToCreateAProduct.class))).thenReturn(mockedResponseFromBusinessAction);
    }

    @Test
    public void shouldReturnCREATEDWhenCreatingAValidExample() {
        CreateNewProductRequest request = new CreateNewProductRequest("Example Product");
        ResponseEntity<CreateNewProductResponse> response = productController.create(request);
        assertThat(response.getStatusCode(), is(CREATED));
    }

    @Test
    public void shouldReturnAValidResponseWhenTheCallingIsSuccessful() {
        CreateNewProductRequest request = new CreateNewProductRequest("Example Product");
        ResponseEntity<CreateNewProductResponse> response = productController.create(request);
        assertThat(response.getBody(), is(instanceOf(CreateNewProductResponse.class)));
    }

    @Test
    public void theResponseShouldHaveTheSameRequestedNameWhenTheCallingIsSuccessful() {
        when(mockedResponseFromBusinessAction.getName()).thenReturn("Example Product");

        CreateNewProductRequest request = new CreateNewProductRequest("Example Product");
        ResponseEntity<CreateNewProductResponse> response = productController.create(request);

        assertThat(response.getBody().getName(), is(request.getName()));
    }

    @Test
    public void shouldCallActOnFromBusinessActionWhenCreatingANewExample() {
        CreateNewProductRequest request = new CreateNewProductRequest("Example Product");
        ResponseEntity<CreateNewProductResponse> response = productController.create(request);

        verify(createNewProductBusinessAction).actOn(request);
    }
}
