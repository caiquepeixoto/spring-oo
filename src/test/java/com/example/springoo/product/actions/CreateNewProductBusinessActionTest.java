package com.example.springoo.product.actions;

import com.example.springoo.product.domain.ProductJPA;
import com.example.springoo.product.domain.InterfaceToCreateAProduct;
import com.example.springoo.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CreateNewProductBusinessActionTest {

    @Mock
    private ProductRepository repository;

    @Mock
    private ProductJPA mockedProductEntity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCallSaveFromRepositoryWhenActing(){
        when(repository.save(any(ProductJPA.class))).thenReturn(mockedProductEntity);

        InterfaceToCreateAProduct productable = mock(InterfaceToCreateAProduct.class);

        CreateNewProductBusinessAction businessAction = new CreateNewProductBusinessAction(repository);
        businessAction.actOn(productable);

        verify(repository).save(any(ProductJPA.class));
    }

}